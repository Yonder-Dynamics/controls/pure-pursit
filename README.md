# Path Following

Install instructions

1. Clone the project
```
git clone https://gitlab.com/Yonder-Dynamics/controls/pure-pursit.git
```

2. Install requirements
```
pip3 install -r requirements.txt
```

3. Go to ~/catkin_ws and build
```
cd ~/catkin_ws && catkin_make
```

4. Source
```
source ~/catkin_ws/devel/setup.zsh
```

To launch `./start.sh`
