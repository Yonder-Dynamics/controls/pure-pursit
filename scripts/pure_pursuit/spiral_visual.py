import numpy as np
import matplotlib.pyplot as plt
import math


def spiral(centerX, centerY, radius, coils, step, rotation=1):
    pts = []
    # value of theta corresponding to end of last coil
    thetaMax = coils * 2 * math.pi
    rotation *= 2*math.pi

    # How far to step away from center for each side.
    awayStep = radius / thetaMax

    # distance between points to plot
    chord = step

    centerX, centerY = 0, 0

    pts.append([centerX, centerY])

    # For every side, step around and away from center.
    # start at the angle corresponding to a distance of chord
    # away from centre.
    theta = chord/awayStep
    while theta <= thetaMax:
        # How far away from center
        away = awayStep * theta
        # How far around the center.
        around = theta + rotation

        # Convert 'around' and 'away' to X and Y.
        x = centerX + math.cos(around) * away
        y = centerY + math.sin(around) * away

        # Now that you know it, do it.
        pts.append([x, y])

        # to a first approximation, the points are on a circle
        # so the angle between them is chord/radius
        # delta = (-2 * away + math.sqrt(4 * away * away +
        #          8 * awayStep * chord)) / (2 * awayStep)
        theta += chord/away
    return pts


sp = spiral(0, 0, 20, 5, 2)
plt.scatter(*zip(*sp))
plt.show()
