import sys
from time import time
from typing import List

import numpy as np

from PurePursuitController import PurePursuit
from TrajectoryGeneration import Segment
from Util import Pose


class CombinedDriveController(object):
    pure_pursuit: PurePursuit = None
    left_right_traj: List[List[Segment]] = None
    max_vel = 0
    lookahead_pose = []
    closest_seg = []
    last_time = time()

    def __init__(self, pure_pursuit_controller: PurePursuit, left_right_traj: List[List[Segment]], max_vel):
        self.pure_pursuit = pure_pursuit_controller
        self.left_right_traj = left_right_traj
        self.max_vel = max_vel

    def calc_combined_output(self, robot_pose: Pose):
        pp = self.pure_pursuit
        pp.update_pose(robot_pose)
        closest_idx = pp.closest_point_idx()
        self.closest_seg = pp.seg_list[closest_idx]
        vel = self.left_right_traj[2][closest_idx].vel
        self.lookahead_pose = pp.calc_lookahead_point(vel)
        # print(
        #     "dt: " + str(int(self.dt())) + " POSE: " + str(robot_pose) + " LOOKAHEAD: "
        #     + str(self.lookahead_pose) + " lookahead dist: " + str(
        #         max(pp.lookahead_at_max_vel * (vel / self.max_vel), pp.minimum_lookahead)),
        #     file=sys.stderr)
        left_right_vel = pp.left_right_speeds(vel, pp.curvature_to_lookahead(self.lookahead_pose, vel))
        left = (left_right_vel[0] + self.left_right_traj[0][closest_idx].vel) / 2
        right = (left_right_vel[1] + self.left_right_traj[1][closest_idx].vel) / 2
        # left = self.left_right_traj[0][closest_idx].vel
        # right = self.left_right_traj[1][closest_idx].vel
        left = np.sign(left) * min(abs(left), self.max_vel)
        right = np.sign(right) * min(abs(right), self.max_vel)
        return [left, right]

    def is_finished(self, points_till_end) -> bool:
        return self.pure_pursuit.closest_point_idx() > len(self.pure_pursuit.seg_list) - points_till_end - 1

    def dt(self):
        cur_time = time()
        delta = (cur_time - self.last_time)/1000
        self.last_time = cur_time
        return delta
