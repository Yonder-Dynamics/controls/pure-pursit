#!/usr/bin/env python3
import sys
import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray, Float64, String
from sensor_msgs.msg import NavSatFix
from time import time
import math
from collections import deque
import numpy as np
from threading import Lock

from pp import PurePursuit
from gps_to_enu import GPS_2_ENU
from spiral_generation import get_spiral

STEERING_CONST = 50
AUTON_SPEED = 50
RATE = 20  # update 30 times a second
HIST_POSE_LEN = 100  # keep these number of history poses
SPIN_SPEED = 45
SPIN_TIME = 25  # how long we will spin around in seconds
AR_TIMEOUT = 5  # how long we wait for new ar tag in seconds
MAX_AUTON_SPEED = 120
MAX_UNSTUCK_SPEED = 50

cur_gps = None
cur_pose = None  # 3 tuple x,y,orientation
gps_history = deque(maxlen=HIST_POSE_LEN * 10)
pose_history = deque(maxlen=HIST_POSE_LEN)
last_gps_point = None  # store last gps point to be able to go back to it
nxt_pose = None  # 3 tuple x,y,orientation
gps_to_xy_converter = None
cur_speed = 0
unstuck_speed = 0
controller = None
auton_publisher = None
set_state_publisher = None
last_time = 0
last_ar_tag = 0
end_spin_time = 0  # reset everytime we enter the SPIN_STATE

mutex = Lock()
in_validation_phase = False
should_look_for_ar_tag = False
target_id = 1

GO_TO_GPS_STATE = 0
SPIN_STATE = 1
SPIRAL_STATE = 2
GO_TO_AR_STATE = 3
FINISHED_STATE = 4

autonomous_state = GO_TO_GPS_STATE

ar_cX, ar_cY, ar_height, ar_ID = 0, 0, 0, 0

def clamp(num, minNum, maxNum):
    return max(min(num, maxNum), minNum)

def set_state(state):
    global autonomous_state
    autonomous_state = state


def on_new_gps_msg(msg):
    """this updates our current position"""
    global cur_pose, cur_gps, cur_speed, last_time, gps_to_xy_converter

    cur_gps = (msg.latitude, msg.longitude)

    if cur_pose is None:
        cur_pose = [0] * 3  # initialize
        gps_to_xy_converter = GPS_2_ENU(*cur_gps)

    new_xy = gps_to_xy_converter.gps_to_xy(*cur_gps)  # convert to xy
    new_time = time()  # get time
    mutex.acquire()
    # calc position change
    diff = np.array(new_xy) - np.array(cur_pose[:2])
    cur_speed = np.linalg.norm(
        diff / (new_time - last_time))  # update the speed
    cur_pose[:2] = new_xy
    # push the currunt pose onto history
    pose_history.append(cur_pose.copy())
    gps_history.append(cur_gps)
    mutex.release()
    last_time = new_time


def on_new_heading_msg(msg):
    global cur_pose
    # print("HEADING", (msg.data*math.pi/180.0)-math.pi/2)
    if cur_pose is not None:
        heading = -(msg.data - 90)  # use east as center and flip
        if heading > 180:
            heading -= 360
        mutex.acquire()
        cur_pose[2] = heading * math.pi / 180
        mutex.release()


def set_new_target(lat, lon):
    global cur_pose, nxt_pose, controller, last_gps_point, gps_history
    if cur_pose is None:
        return  # don't have localization yet
    set_state(GO_TO_GPS_STATE)

    gps_history.clear()
    next_xy = gps_to_xy_converter.gps_to_xy(lat, lon)
    last_gps_point = cur_gps  # save this so that we can return back to it
    diff = np.array(next_xy) - np.array(cur_pose[:2])
    next_orientation = math.atan2(diff[1], diff[0])
    nxt_pose = [*next_xy, next_orientation]
    controller = PurePursuit(
        pts=[cur_pose[:2], nxt_pose[:2]
             ], start_ang=cur_pose[2], end_ang=nxt_pose[2]
    )
    print(cur_pose, nxt_pose, diff)


def on_receive_new_gps_waypoint(twist):
    """we are using twist to transport gps lat and lon right now"""
    global should_look_for_ar_tag
    global target_id
    global AUTON_SPEED

    # TODO: real solution
    should_look_for_ar_tag = twist.linear.z  # jank solution but works
    target_id = twist.angular.x  
    AUTON_SPEED = twist.angular.y
    set_new_target(twist.linear.x, twist.linear.y)


def found_ar_tag(msg):
    global ar_cX, ar_cY, ar_height, ar_ID, last_ar_tag
    [ar_cX, ar_cY, ar_height, ar_ID] = msg.data
    if ar_ID != target_id:
        return
    last_ar_tag = time()
    if autonomous_state != FINISHED_STATE and autonomous_state != GO_TO_GPS_STATE:
        set_state(GO_TO_AR_STATE)


def init_publishers_and_subscribers():
    global auton_publisher, set_state_publisher
    auton_publisher = rospy.Publisher(
        "/autonomous_drive_train", Float64MultiArray, queue_size=1
    )
    set_state_publisher = rospy.Publisher("/set_state", String, queue_size=1)
    rospy.Subscriber("/mavros/global_position/global",
                     NavSatFix, on_new_gps_msg)
    rospy.Subscriber("/mavros/global_position/compass_hdg",
                     Float64, on_new_heading_msg)
    rospy.Subscriber("/rover_waypoint", Twist, on_receive_new_gps_waypoint)
    rospy.Subscriber("/artag_position", Float64MultiArray, found_ar_tag)


def follow_pure_pursuit(vel_msg):
    global controller, cur_pose, nxt_pose, cur_speed, in_validation_phase, end_spin_time, unstuck_speed

    # if we don't have the controller initialized or don't have a gps fix, then do nothing
    if controller is None:
        return
    elif cur_pose is None:
        return

    if controller.did_reach_goal():  # check if we are within tolerance
        print(len(pose_history))
        if not in_validation_phase:  # if we just entered validation
            in_validation_phase = True
            pose_history.clear()  # clear past data
        # entered validation phase and collected enough data
        elif len(pose_history) == HIST_POSE_LEN:
            in_validation_phase = False
            mutex.acquire()
            cur_pose = list(np.array(pose_history).mean(axis=0))
            mutex.release()
            if controller.check_should_redo_goal(cur_pose):
                pass  # goal not reached, redo
            # if within tolerance and need to look for ar_tag
            elif should_look_for_ar_tag:
                end_spin_time = time() + SPIN_TIME
                set_state(SPIN_STATE)
            else:
                set_state(FINISHED_STATE)
    # we failed to turn quickly enough, so trying again from here
    elif controller.is_outside_lookahead():
        controller = PurePursuit(
            pts=[cur_pose[:2], nxt_pose[:2]
                 ], start_ang=cur_pose[2], end_ang=nxt_pose[2]
        )
        print("making a new controller since we didn't turn fast enough")
    else:  # just following things normally
        if is_stuck():
            unstuck_speed += 0.5
            unstuck_speed = min(unstuck_speed, MAX_UNSTUCK_SPEED)
        else:
            unstuck_speed = 0

        steering_radius = controller.calc_steering(cur_pose)
        steering_output = STEERING_CONST * cur_speed / steering_radius
        vel_msg.data = [AUTON_SPEED - steering_output + unstuck_speed,
                        AUTON_SPEED + steering_output + unstuck_speed]
        print(
            "POSE",
            list(map(lambda x: round(x, 1), cur_pose)),
            "STEERING",
            round(steering_radius, 3),
            "GPS AVG",
            list(map(lambda x: round(x, 6), np.array(gps_history).mean(axis=0))),
        )


def spin(vel_msg):
    global controller
    print(f"spinning for {round(end_spin_time-time())} more seconds")
    vel_msg.data = [-SPIN_SPEED, SPIN_SPEED]
    if time() > end_spin_time:  # done with spinning
        pts = get_spiral(cur_pose[0], cur_pose[1], radius=10, coils=4, step=2)
        pts = pts[3:]
        end_ang = math.atan((pts[-1][1] - pts[-2][1]) /
                            (pts[-1][0] - pts[-2][0]))
        controller = PurePursuit(
            pts=pts, start_ang=cur_pose[2], end_ang=end_ang)
        set_state(SPIRAL_STATE)  # go to spiral if we don't see the tag


def spiral(vel_msg):
    global should_look_for_ar_tag

    print("Spiraling")
    if controller.did_reach_goal():  # no tag found, go home
        should_look_for_ar_tag = False
        print(last_gps_point)
        set_new_target(*last_gps_point)
    else:  # proceed as usual
        steering_radius = controller.calc_steering(cur_pose)
        steering_output = STEERING_CONST * cur_speed / steering_radius
        vel_msg.data = [AUTON_SPEED - steering_output,
                        AUTON_SPEED + steering_output]
        print(
            "POSE",
            list(map(lambda x: round(x, 1), cur_pose)),
            "STEERING",
            round(steering_radius, 3),
            "GPS AVG",
            list(map(lambda x: round(x, 6), np.array(gps_history).mean(axis=0))),
        )


def go_to_ar(vel_msg):
    global ar_cX, ar_height, end_spin_time

    if time() - last_ar_tag > AR_TIMEOUT:
        end_spin_time = time() + end_spin_time
        set_state(SPIRAL_STATE)
        return

    # if the AR tag is big enough in frame, must be close enough
    if abs(ar_height) > 0.062:
        vel_msg.data = [0, 0]
        set_state(FINISHED_STATE)

    off_center = ar_cX - 0.5
    vel_msg.data = [
        AUTON_SPEED / 3 + off_center * STEERING_CONST * 3,
        AUTON_SPEED / 3 - off_center * STEERING_CONST * 3,
    ]

def get_auto_vel():
    global gps_history
    if len(gps_history) > 20:
        vel = math.sqrt(sum([(gps_history[-19][i] - gps_history[-1][i])**2 for i in range(2)]))
        return vel
    else:
        return 1

def is_stuck():
    return get_auto_vel() < 5 * (10 ** (-6))

def send():
    vel_msg = Float64MultiArray()
    vel_msg.data = [0] * 2  # these are the left and right speeds

    rate = rospy.Rate(RATE)  # update freq

    finish_msg_published = False
    while not rospy.is_shutdown():
        rate.sleep()
        vel_msg.data = [0, 0]  # left and right drive speed set to 0 first
        # reached goal
        if autonomous_state == GO_TO_GPS_STATE:
            finish_msg_published = False
            follow_pure_pursuit(vel_msg)
        elif autonomous_state == SPIN_STATE:
            spin(vel_msg)
        elif autonomous_state == SPIRAL_STATE:
            spiral(vel_msg)
        elif autonomous_state == GO_TO_AR_STATE:
            go_to_ar(vel_msg)
        elif autonomous_state == FINISHED_STATE and not finish_msg_published:
            finished = String()
            finished.data = "goal_reached"
            set_state_publisher.publish(finished)
            finish_msg_published = True
        else:
            print("chilling")

        for i in range(len(vel_msg.data)):
            vel_msg.data[i] = clamp(vel_msg.data[i], -MAX_AUTON_SPEED, MAX_AUTON_SPEED)
        auton_publisher.publish(vel_msg)


if __name__ == "__main__":
    rospy.init_node("purepursuit")
    init_publishers_and_subscribers()
    try:
        send()
    except rospy.ROSInterruptException:
        print("ros interruption received", file=sys.stderr)
        pass
