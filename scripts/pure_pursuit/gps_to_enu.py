# Import geonav tranformation module
import geonav_transform.geonav_conversions as gc
# reload(gc)
# Import AlvinXY transformation module
import alvinxy.alvinxy as axy
# reload(axy)


class GPS_2_ENU():
    def __init__(self, olat, olon):
        self.olat = olat
        self.olon = olon

    def gps_to_xy(self, lat, lon):
        xg2, yg2 = gc.ll2xy(lat, lon, self.olat, self.olon)
        return xg2, yg2
        # return yg2, -xg2
